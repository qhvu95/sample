FROM dockerhub.vtgo.vn:5000/wrapper:0.0.1
RUN rm -rf /home/wrapper/etc/*
WORKDIR /home/wrapper/bin/
COPY target/lib /home/wrapper/lib/lib
COPY target/*.jar /home/wrapper/lib
COPY etc /home/wrapper/etc
EXPOSE 8888
ENTRYPOINT /home/wrapper/bin/process start && tail -f /home/wrapper/logs/wrapper.log
