/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.config;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 *
 * @author MyPC
 */
public class Config {
    
    private static final Logger logger = Logger.getLogger(Config.class);
    
    public static String folderUploadImage = "/u02/IMAGE/";
    
    public static void loadConfig(String path) {
        FileReader fr = null;
        Properties pro = null;
        folderUploadImage = pro.getProperty("folderUploadImage", "/u02/IMAGE/").trim();
        try {
            fr = new FileReader(path);
            pro = new Properties();
            pro.load(fr);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            if (pro != null) {
                pro.clear();
            }
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
    }
}
