package com.vinatek.base.security;

import java.util.Map;
import java.util.Optional;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

/**
 * @author thanh
 */
public class SecurityContextHelper {

    public static UserLogin getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof UserAuthenticationToken) {
            return extractUserLogin((UserAuthenticationToken) authentication);
        }
        if (authentication instanceof OAuth2Authentication) {
            return extractUserLogin((OAuth2Authentication) authentication);
        }
        return null;
    }

    public static Long getCurrentUserId() {
        return SecurityContextHelper.getCurrentUser().getId();
    }

    public static boolean isUserHasRole(String role) {
        UserLogin userLogin = getCurrentUser();
        if (userLogin == null) {
            return false;
        }
        return (userLogin.getRole() == null ? role == null : userLogin.getRole().equals(role));
    }

    public static UserLogin extractUserLogin(UserAuthenticationToken auth) {
        if (auth == null) {
            return null;
        }
        return auth.getUserLogin();
    }

    public static UserLogin extractUserLogin(OAuth2Authentication auth) {
        if (auth == null) {
            return null;
        }
        Long userId = getLongValue(auth.getPrincipal());
        
        Optional<String> role = auth
                .getAuthorities()
                .stream()
                .map(authority -> authority.getAuthority().substring(5))
                .filter(authority -> !"USER".equals(authority))
                .findFirst();
        
        OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) auth.getDetails();
        @SuppressWarnings("unchecked")
        Map<String, Object> detailsMap = (Map<String, Object>) details.getDecodedDetails();
        String username = (String) detailsMap.get("username");
        
        return new UserLogin(role.get(), userId, username);
    }

    /**
     * Check if user already logged via login page
     * @return 
     */
    public static boolean isLoggedIn() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof UserAuthenticationToken;
    }
    
    private static Long getLongValue(Object obj) {
        if (obj instanceof Long) {
            return (Long) obj;
        }
        if (obj instanceof Number) {
            return ((Number) obj).longValue();
        }
        if (obj instanceof String) {
            return Long.valueOf(obj.toString());
        }
        return null;
    }

}
