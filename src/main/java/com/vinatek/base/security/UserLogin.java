package com.vinatek.base.security;

import com.vinatek.base.constant.ActiveStatus;
import com.vinatek.base.persistent.domain.User;
import java.io.Serializable;
import lombok.Data;

@Data
public class UserLogin implements Serializable {

    private static final long serialVersionUID = -7942635748301089503L;

    private Long id;
    private String username;
    private String role;
    private String name;
    private int activeStatus;
    private String fcmToken;
    private String phone;

    public UserLogin() {
        super();
    }

    public UserLogin(User domain) {
        this.id = domain.getId();
        this.username = domain.getUsername();
        this.role = domain.getUserRole().name();
        this.name = domain.getName();
        this.activeStatus = domain.getActiveStatus();
        this.fcmToken = domain.getFcmToken();
        this.phone = domain.getPhone();
    }

    public UserLogin(String role, Long id, String username) {
        super();

        this.role = role;
        this.id = id;
        this.username = username;
    }

    public boolean isRole(String... roles) {
        if (this.role != null && roles != null) {
            for (String role : roles) {
                if (this.role.equals(role)) {
                    return true;
                }
            }
        }

        return false;
    }
}
