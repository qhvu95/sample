package com.vinatek.base.endpoint.base;

import com.vinatek.base.dto.base.DayOfWeekDto;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Hoa_dev
 */
@RestController
@RequestMapping(value = "/api/common")
public class CommonEndpoint extends AbstractEndpoint {

    @GetMapping(value = "/day-of-week")
    public ResponseEntity<?> getListDayOfWeek() {
        List<DayOfWeekDto> lstDay = new ArrayList<>();
        lstDay.add(new DayOfWeekDto(1L, "Chủ nhật"));
        lstDay.add(new DayOfWeekDto(2L, "Thứ hai"));
        lstDay.add(new DayOfWeekDto(3L, "Thứ ba"));
        lstDay.add(new DayOfWeekDto(4L, "Thứ tư"));
        lstDay.add(new DayOfWeekDto(5L, "Thứ năm"));
        lstDay.add(new DayOfWeekDto(6L, "Thứ sáu"));
        lstDay.add(new DayOfWeekDto(7L, "Thứ bảy"));
        return getResponseEntity(lstDay);
    }

    @GetMapping(value = "/shifts")
    public ResponseEntity<?> getShifts() {
        List<DayOfWeekDto> lstShifts = new ArrayList<>();
        lstShifts.add(new DayOfWeekDto(1L, "Ca sáng"));
        lstShifts.add(new DayOfWeekDto(2L, "Ca chiều"));
        lstShifts.add(new DayOfWeekDto(3L, "Ca tối"));
        return getResponseEntity(lstShifts);
    }
}
