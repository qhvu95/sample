package com.vinatek.base.endpoint.base;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vinatek.base.dto.base.FileDownloadDto;
import com.vinatek.base.security.SecurityContextHelper;
import com.vinatek.base.security.UserLogin;
import com.vinatek.base.utils.Envelope;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public abstract class AbstractEndpoint {

    public static final String ADMIN = "admin";
    public static final String SUB_ADMIN = "sub-admin";
    public static final String PARTNER_ADMIN = "partner-admin";
    public static final String STORE_ADMIN = "store-admin";
    public static final String PUBLIC = "public";
    public static final String ANY = "any";

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final UserLogin getUserLogin() {
        return SecurityContextHelper.getCurrentUser();
    }

    protected final ResponseEntity<?> getResponseEntity(Object data) {
        return new Envelope(data).toResponseEntity(HttpStatus.OK);
    }

    protected final ResponseEntity<InputStreamResource> getFileDownloadResponseEntity(FileDownloadDto fileDownloadDto) {
        InputStreamResource response;
        try {
            response = new InputStreamResource(new FileInputStream(fileDownloadDto.getFile()));
        } catch (FileNotFoundException e) {
            logger.error("file not found", e);
            throw new UnsupportedOperationException(e);
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        header.set("Content-Disposition", "attachment; filename=\"" + fileDownloadDto.getFileName() + "\"");
        header.setContentDispositionFormData(fileDownloadDto.getFileName(), fileDownloadDto.getFileName());

        return new ResponseEntity<>(response, header, HttpStatus.OK);
    }

    protected final ResponseEntity<InputStreamResource> getFilePdfResponseEntity(FileDownloadDto fileDownloadDto) {
        InputStreamResource response;
        try {
            response = new InputStreamResource(new FileInputStream(fileDownloadDto.getFile()));
        } catch (FileNotFoundException e) {
            logger.error("file not found", e);
            throw new UnsupportedOperationException(e);
        }

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_PDF);
//        header.set("Content-Disposition", "inline; filename=\"" + fileDownloadDto.getFileName() + "\"");
        header.setContentDispositionFormData(fileDownloadDto.getFileName(), fileDownloadDto.getFileName());

        return new ResponseEntity<>(response, header, HttpStatus.OK);
    }

    protected <T extends Serializable> T parseJson(Class<T> clazz, String json) {
        try {
            return new ObjectMapper().readValue(json, clazz);
        } catch (IOException e) {
            throw new HttpMessageNotReadableException("Data format is invalid", (HttpInputMessage) null);
        }
    }

}
