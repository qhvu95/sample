package com.vinatek.base.endpoint;

import com.vinatek.base.dto.user.UserWriteDto;
import com.vinatek.base.endpoint.base.AbstractEndpoint;
import com.vinatek.base.service.management.UserService;
import com.vinatek.base.utils.Meta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Khiem Ha
 */
@RestController
@RequestMapping(value = "/api/users")
public class UsersEndpoint extends AbstractEndpoint {

    @Autowired
    UserService userService;

    @GetMapping(value = "")
    public ResponseEntity<?> getInfo() {
        return getResponseEntity(userService.getCurrentUser());
    }

    // Tạo tài khoản khách hàng
    @PostMapping(value = "/register")
    public ResponseEntity<?> registerUser(@RequestBody UserWriteDto request) {
        return getResponseEntity(userService.registerCustomer(request));
    }

    // Tạo tài khoản staff
    @PostMapping(value = "/create-staff")
    public ResponseEntity<?> createUser(@RequestBody UserWriteDto request) {
        return getResponseEntity(userService.create(request));
    }

    @PostMapping(value = "/update-user")
    public ResponseEntity<?> updateUser(@RequestBody UserWriteDto request) {
        return getResponseEntity(userService.updateUser(request));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return getResponseEntity(userService.getById(id));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        userService.delete(id);
        return getResponseEntity(Meta.OK);
    }

    @PostMapping(value = "/change-password")
    public ResponseEntity<?> changePassword(@RequestBody UserWriteDto request) {
        return getResponseEntity(userService.changePassword(request));
    }

    @PostMapping(value = "/update-fcm-token")
    public ResponseEntity<?> updateFcmToken(@RequestBody UserWriteDto request) {
        return getResponseEntity(userService.updateFcmToken(request));
    }

    @GetMapping(value = "/{id}/reset-password")
    public ResponseEntity<?> changePassword(@PathVariable Long id) {
        return getResponseEntity(userService.resetPassword(id));
    }
}
