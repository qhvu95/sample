package com.vinatek.base.utils.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.vinatek.base.utils.Envelope;
import com.vinatek.base.utils.RestError;
import org.apache.log4j.Logger;

/**
 * @author thanh
 */
@ControllerAdvice
public class DefaultExceptionHandler {

//    protected Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final Logger LOGGER = Logger.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<?> defaultErrorHandler(HttpServletRequest req, Exception e) {
        LOGGER.error("Unknow error", e);
        RestError error = new RestError("unknown", 500, "Unknown error occurs");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public ResponseEntity<?> notFoundErrorHandler(HttpServletRequest req, NoHandlerFoundException e) {
//        LOGGER.debug("ResourceNotFoundException error", e);
        RestError error = new RestError("ResourceNotFoundException", 404, "Requested resource not found");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ResponseEntity<?> illegalArgumentExceptionHandler(HttpServletRequest req, IllegalArgumentException e) {
        LOGGER.debug("IllegalArgumentException error", e);
        RestError error = new RestError("IllegalArgumentException", 400, BusinessExceptionCode.INVALID_PARAM);
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BusinessException.class)
    public ResponseEntity<?> businessErrorHandler(HttpServletRequest req, BusinessException e) {
//        LOGGER.debug("BusinessException error", e);
        RestError error = new RestError("BusinessException", 400, e.getCode());
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = JsonMappingException.class)
    public ResponseEntity<?> jsonErrorHandler(HttpServletRequest req, JsonMappingException e) {
//        LOGGER.debug("JSON mapping error", e);
        RestError error = new RestError("IllegalArgumentException", 400, "Invalid request parameter");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<?> methodNotSupportErrorHandler(HttpServletRequest req, Exception e) {
//        LOGGER.debug("MethodNotSupportedException error", e);
        RestError error = new RestError("MethodNotSupportedException", 400, "Method not supported");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class, MissingServletRequestParameterException.class, HttpMessageNotReadableException.class,
        MethodArgumentTypeMismatchException.class})
    public ResponseEntity<?> illegalErrorHandler(HttpServletRequest req, Exception e) {
        LOGGER.debug("IllegalArgument error", e);
        RestError error = new RestError("IllegalArgumentException", 400, "Invalid request parameter");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<?> mediaTypeNotSupportErrorHandler(HttpServletRequest req, Exception e) {
//        LOGGER.debug("HttpMediaTypeNotSupported error", e);
        RestError error = new RestError("MediaTypeNotSupportedException", 400, "MediaType not supported");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> accessDeniedErrorHandler(HttpServletRequest req, AccessDeniedException e) {
//        LOGGER.debug("AccessDeniedException error", e);
        RestError error = new RestError("AccessDeniedException", 401, "Access denied");
        Envelope response = new Envelope(error);
        return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
    }

}
