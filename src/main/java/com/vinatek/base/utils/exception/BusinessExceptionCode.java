package com.vinatek.base.utils.exception;

public class BusinessExceptionCode {

    // BASE
    public static final String INVALID_PARAM = "invalid.param";
    public static final String INVALID_EXCEL = "invalid.excel";
    public static final String INVALID_EMAIL = "invalid.email";
    public static final String INVALID_PHONE = "invalid.phone";
    public static final String INVALID_USERNAME = "invalid.username";
    public static final String INVALID_ROLE = "invalid.role";
    public static final String INVALID_NAME = "invalid.name";
    public static final String INVALID_LINK = "invalid.link";
    public static final String UNSUPPORTED_IMAGE = "unsupported.image";
    public static final String INVALID_IMAGE_SIZE = "invalid.image.size";
    public static final String INVALID_OLD_PASSWORD = "invalid.old.password";
    public static final String INVALID_NEW_PASSWORD = "invalid.new.password";
    public static final String INVALID_CONFIRM_PASSWORD = "invalid.confirm.password";
    public static final String INVALID_CODE = "invalid.code";
    public static final String INVALID_STATUS = "invalid.status";
    public static final String INVALID_IMAGE = "image.error";
    public static final String INVALID_ORDER_STATUS = "invalid.order.status";
    public static final String INVALID_CART_DATA_STATUS = "invalid.cart.data.status";
    public static final String INVALID_ORDER_DATA_STATUS = "invalid.order.data.status";
    public static final String INVALID_PACKAGE_STATUS = "invalid.package.status";
    public static final String INVALID_ORDER_DATA_ID = "invalid.order.data.id";
    public static final String INVALID_BARGAIN_VALUE = "invalid.bargain.value";

    public static final String ID_NOT_EXIST = "id.not.exist";
    public static final String ENTITY_NOT_FOUND = "entity.not.found";
    public static final String RECHARGE_CODE_NOT_FOUND = "recharge.code.not.found";
    public static final String SETTING_NOT_FOUND = "setting.not.found";
    public static final String USERNAME_NOT_FOUND = "username.not.found";
    public static final String REFERRAL_NOT_FOUND = "referral.not.found";
    public static final String CART_NOT_FOUND = "cart.not.found";
    public static final String CART_SIZE_LIMITED = "cart.size.limited";
    public static final String CART_DATA_NOT_FOUND = "cart.data.not.found";
    public static final String ORDER_NOT_FOUND = "order.not.found";
    public static final String PACKAGE_NOT_FOUND = "package.not.found";
    public static final String ORDER_HISTORY_NOT_FOUND = "order.history.not.found";
    public static final String EXCEED_BALANCE = "exceed.balance";
    public static final String DEPOSIT_AMOUNT_NOT_ENOUGH = "deposit.amount.not.enough";
    public static final String REACHED_LIMIT = "reached.limit";

    public static final String USERNAME_USED = "username.used";
    public static final String NAME_USED = "name.used";
    public static final String CODE_USED = "code.used";
    public static final String WAREHOUSE_NOT_FOUND = "warehouse.not.found";
    public static final String WAREHOUSE_HAS_CUSTOMER = "warehouse.has.customer";
    public static final String CUSTOMER_NOT_FOUND = "customer.not.found";
    public static final String BANK_NOT_FOUND = "bank.not.found";
    public static final String MOBILE_USED = "mobile.used";
    public static final String EMAIL_USED = "email.used";
    public static final String PHONE_EXIST = "phone.exist";
    public static final String ORIGINAL_INVOICE_EXIST = "original.invoice.exist";
    public static final String PACKAGE_EXIST = "package.exist";

    // ROLE CHECK
    public static final String PERMISSION_DENIED = "permission.denied";
}
