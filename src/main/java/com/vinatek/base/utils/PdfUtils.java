package com.vinatek.base.utils;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.vinatek.base.dto.base.FileDownloadDto;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import org.springframework.util.Assert;

/**
 *
 * @author Khiem Ha
 */
public class PdfUtils {

    public static final FileDownloadDto getFileDownloadDto(Document doc, PdfPTable table, List<Paragraph> listParas, String fileName) {
        Assert.notNull(fileName, "fileName cannot be null");

//        fileName = StringUtils.getSearchableString(fileName).replace(" ", "_");
        File outTempFile = getFile(doc, table, listParas);
        return new FileDownloadDto(fileName, outTempFile);
    }

    public static final File getFile(Document doc, PdfPTable table, List<Paragraph> listParas) {
        try {
            File outTempFile = File.createTempFile(UUID.randomUUID().toString(), "tmp");
            try (FileOutputStream outputStream = new FileOutputStream(outTempFile)) {
                PdfWriter.getInstance(doc, outputStream);
                doc.open();
                for (Paragraph para : listParas) {
                    doc.add(para);
                }
                doc.add(table);
                doc.close();
                return outTempFile;
            } catch (DocumentException ex) {
                throw new UnsupportedOperationException(ex);
            }
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}
