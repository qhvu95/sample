package com.vinatek.base.utils;

import com.vinatek.base.utils.CodeConfig.Charset;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class CodeUtils {
    private static final Random RND = new Random(System.currentTimeMillis());

    public static String generate(CodeConfig config) {
        StringBuilder sb = new StringBuilder();
        char[] chars = config.getCharset().toCharArray();
        char[] pattern = config.getPattern().toCharArray();

        if (config.getPrefix() != null) {
            sb.append(config.getPrefix());
        }

        for (int i = 0; i < pattern.length; i++) {
            if (pattern[i] == CodeConfig.PATTERN_PLACEHOLDER) {
                sb.append(chars[RND.nextInt(chars.length)]);
            } else {
                sb.append(pattern[i]);
            }
        }

        if (config.getPostfix() != null) {
            sb.append(config.getPostfix());
        }

        return sb.toString();
    }
    
    public static String generateCodeOfGivenLength(Integer length) {   
        
        if (length == null || length == 0) {
            return null;
        }

        CodeConfig config = CodeConfig.length(length).withCharset(Charset.CODE);
        
        return CodeUtils.generate(config);
    }

    public static Set<String> generateCodeOfGivenLength(Integer length, Long numberOfCodes, Set<String> usedCode) {        
        Set<String> result = new HashSet<>();
        
        if (usedCode == null) {
            usedCode = new HashSet<>();
        }
        
        if (length == null || length == 0) {
            return result;
        }
        
        CodeConfig config = CodeConfig.length(length).withCharset(Charset.CODE);
        
        if (numberOfCodes != null && numberOfCodes > 0) {
            String code = null;
            for (long i = 0; i < numberOfCodes; i++) {
                code = CodeUtils.generate(config);
                if (usedCode.contains(code)) {
                    code = CodeUtils.generate(config);
                }
                result.add(code);
                usedCode.add(code);
            }
        }        
        return result;
    }
}
