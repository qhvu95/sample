package com.vinatek.base.utils;

import org.springframework.util.DigestUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MyStringUtils {
    
    public static String getHash(String str) {
        String strHex = DigestUtils.md5DigestAsHex(str.getBytes());
        return strHex;
    }
    
    public static String genOrderCode(Long orderId, Long customerId) {
        StringBuilder pattern = new StringBuilder("000000");
        String strOrderId = orderId.toString();
        
        StringBuilder result = new StringBuilder("TM");
        result.append(customerId).append("-");
        if (strOrderId.length() >= 6) {
            result.append(strOrderId);
        } else {
            int length = strOrderId.length();
            for (int i = 0; i < length; i++) {
                pattern.setCharAt(5 - i, strOrderId.charAt(length - 1 - i));
            }
            
            result.append(pattern.toString());
        }
        
        return result.toString();
    }
    
    public static String genCustomerCode(Long customerId) {
        StringBuilder pattern = new StringBuilder("000000");
        String strCustomerId = customerId.toString();
        
        StringBuilder result = new StringBuilder("TM");
        if (strCustomerId.length() >= 6) {
            result.append(strCustomerId);
        } else {
            int length = strCustomerId.length();
            for (int i = 0; i < length; i++) {
                pattern.setCharAt(5 - i, strCustomerId.charAt(length - 1 - i));
            }
            
            result.append(pattern.toString());
        }
        
        return result.toString();
    }
    
    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            return convertByteToHex(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String convertByteToHex(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < data.length; i++) {
            sb.append(Integer.toString((data[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
//    
//    public static void main(String[] args) {
//        StringBuilder sb = new StringBuilder();
//        sb.append("TM001;");
//        sb.append("TM002;");
//        
//        System.out.println(sb.substring(0, sb.length() - 1));
//    }
    
    public static String escapeStringForMySQL(String s) {
        return s.replace("\\", "\\\\").replace("\b", "\\b").replace("\n", "\\n").replace("\r", "\\r").replace("\t", "\\t").replace("\\x1A", "\\Z")
                .replace("\\x00", "\\0").replace("'", "\\'").replace("\"", "\\\"");
    }
    
    public static String escapeWildcardsForSQL(String s) {
        return escapeStringForMySQL(s)
                .replace("%", "\\%")
                .replace("_", "\\_");
    }
    
    public static boolean isEmpty(String str) {
        return (str == null || "".equals(str.toString().trim()));
    }
    
}
