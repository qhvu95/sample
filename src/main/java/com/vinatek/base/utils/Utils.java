/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MyPC
 */
public class Utils {
    public static String saveUploadedFiles(MultipartFile file, String dir, Long id) throws IOException {
        if (file.isEmpty()) {
                return null;
        } else {
            if(id != null) {
                String fileName = file.getOriginalFilename();
                fileName = fileName.replace(".", "_" + id + ".");
                byte[] bytes = file.getBytes();
                Path path = Paths.get(dir + fileName);
                Files.write(path, bytes);
                return fileName;
            } else {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(dir + file.getOriginalFilename());
                Files.write(path, bytes);
                return file.getOriginalFilename();
            }
        }
    }
}
