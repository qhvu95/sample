package com.vinatek.base.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.util.Assert;

public class DateTimeUtils {

    private static final String ISO_MONTH_SDF_PATTERN = "yyyy-MM";
    private static final String ISO_DATE_SDF_PATTERN = "yyyy-MM-dd";
    private static final String ISO_TIME_SDF_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
    private static final String DEFAULT_TIME_SDF_PATTERN = "yyyy-MM-dd HH:mm:ss";

    private static final String FORMAT_DATE_TIME = "dd/MM/yyyy HH:mm:ss";
    private static final String FORMAT_DATE = "dd/MM/yy";

    public static Date getCurrentTime() {
        return new Date();
    }

    public static Date getToday() {
        return DateUtils.truncate(getCurrentTime(), Calendar.DATE);
    }

    public static final Long getTime(Date date) {
        if (date == null) {
            return null;
        }

        return date.getTime();
    }

    public static final String getIsoMonth(Date date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(ISO_MONTH_SDF_PATTERN).format(date);
    }

    public static final String getIsoDate(Date date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(ISO_DATE_SDF_PATTERN).format(date);
    }

    public static final String getIsoTime(Date date) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(ISO_TIME_SDF_PATTERN).format(date);
    }

    public static final Date getDefaultTime(String defaultTime) throws ParseException {
        if (defaultTime == null) {
            return null;
        }

        return new SimpleDateFormat(DEFAULT_TIME_SDF_PATTERN).parse(defaultTime);
    }

    public static final Date getDateFromIsoDate(String isoDate) throws ParseException {
        if (isoDate == null) {
            return null;
        }

        return new SimpleDateFormat(ISO_DATE_SDF_PATTERN).parse(isoDate);
    }

    public static final Date getDateFromIsoTime(String isoTime) throws ParseException {
        if (isoTime == null) {
            return null;
        }

        return new SimpleDateFormat(ISO_TIME_SDF_PATTERN).parse(isoTime);
    }

    public static int getField(Date date, int field) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.get(field);
    }

    public static Date getDate(int year, int month, int date) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date, 0, 0, 0);

        return calendar.getTime();
    }

    public static boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) && cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
    }

    public static long getDiffDays(Date date1, Date date2) {
        Assert.notNull(date1, "date1 cannot null");
        Assert.notNull(date2, "date2 cannot null");

        long diff = date2.getTime() - date1.getTime();

        return diff / (24 * 60 * 60 * 1000);
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static Date getEndOfHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }

    public static String formatDateTime(Date date) {
        return DateFormatUtils.format(date, FORMAT_DATE_TIME);
    }

    public static String formatDate(Date date) {
        return DateFormatUtils.format(date, FORMAT_DATE);
    }

    public static String formatTimeVn(Date date) {
        Integer hours = date.getHours();
        Integer minute = date.getMinutes();
        String hoursToString = hours.toString() + "h";
        String minuteToString = minute < 10 ? "0" + minute.toString() : minute.toString();
        return hoursToString + minuteToString;
    }
    
    public static Long getFirstDate() {
        Date date = new Date();
        date.setDate(1);
        date.setMonth(0);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date.getTime();
    }
    
    public static Long getLastDate() {
        Date date = new Date();
        date.setDate(31);
        date.setMonth(11);
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date.getTime();
    }
    
    public static Long getStartDay() {
        Date date = new Date();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date.getTime();
    }
    
    public static Long getEndDay() {
        Date date = new Date();
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date.getTime();
    }
    
    public static Long getDateStartMonth(int month) {
        Date date = new Date();
        date.setMonth(month);
        date.setDate(1);
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        return date.getTime();
    }
    
    public static Long getDateEndMonth(int month) {
        Date date = new Date();
        date.setMonth(month);
        switch(month) {
            case 0: 
                date.setDate(31);
                break;
            case 1:
                if((date.getYear() % 4) == 0) {
                    date.setDate(29);
                } else {
                    date.setDate(28);
                }
                break;
            case 2: 
                date.setDate(31);
                break;
            case 3: 
                date.setDate(30);
                break;
            case 4: 
                date.setDate(31);
                break;
            case 5: 
                date.setDate(30);
                break;
            case 6: 
                date.setDate(31);
                break;
            case 7: 
                date.setDate(31);
                break;
            case 8: 
                date.setDate(30);
                break;
            case 9: 
                date.setDate(31);
                break;
            case 10: 
                date.setDate(30);
                break;
            case 11: 
                date.setDate(31);
                break;
        }
        date.setHours(23);
        date.setMinutes(59);
        date.setSeconds(59);
        return date.getTime();
    }
    
    
}
