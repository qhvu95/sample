package com.vinatek.base.persistent.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vinatek.base.constant.UserRole;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;

/**
 * A user.
 */
@Entity
@Table(name = "users")
@Data
@SuppressWarnings({"PersistenceUnitPresent", "ValidAttributes"})
@EqualsAndHashCode(of = {"id"}, callSuper = false)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
//    @Pattern(regexp = Constants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Column(length = 50, unique = true, nullable = false)
    private String username;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    @Column(name = "password", length = 60, nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole userRole;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "fcm_token")
    private String fcmToken;

    @Column(name = "active_status")
    private int activeStatus;
    
    @CreatedBy
    @Column(name = "created_by")
    private Long createdBy;
    
    @Column(name = "created_at")
    private Long createdAt;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "updated_at")
    private Long updatedAt;

    @Override
    public String toString() {
        return "User{"
                + "username='" + username + '\''
                + ", role='" + userRole + '\''
                + ", activated='" + activeStatus + '\''
                + "}";
    }
}
