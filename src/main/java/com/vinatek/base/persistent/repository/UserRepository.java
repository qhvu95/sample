package com.vinatek.base.persistent.repository;

import com.vinatek.base.constant.UserRole;
import com.vinatek.base.persistent.domain.User;
import java.util.EnumSet;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Khiem Ha
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByUsername(String login);
    
    User findOneByUsernameAndEmail(String username, String email);

    User findOneByEmail(String email);

    User findOneByCode(String code);

    User findOneByCodeAndRole(String code, UserRole role);

    List<User> findAllByRole(EnumSet<UserRole> setRole, String fullname, Pageable pageable);
    
    long count(EnumSet<UserRole> setRole, String fullname);
}
