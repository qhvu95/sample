package com.vinatek.base.persistent.repository.impl;

import com.vinatek.base.constant.UserRole;
import com.vinatek.base.persistent.domain.User;
import com.vinatek.base.persistent.repository.UserRepository;
import com.vinatek.base.utils.MyStringUtils;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 *
 * @author Khiem Ha
 */
@Repository
public class UserRepositoryImpl extends SimpleJpaRepository<User, Long> implements UserRepository {

    public UserRepositoryImpl(EntityManager entityManager) {
        super(User.class, entityManager);
    }

    @Override
    public User findOneByUsername(String login) {
        Specification<User> spec = new ByUsernameSpec(login);

        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public User findOneByUsernameAndEmail(String username, String email) {
        Specification<User> spec = new ByUsernameSpec(username);
        if (!StringUtils.isEmpty(email)) {
            spec = spec.and(new ByEmailSpec(email));
        }
        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public User findOneByEmail(String email) {
        Specification<User> spec = new ByEmailSpec(email);

        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public User findOneByCode(String code) {
        Specification<User> spec = new ByCodeSpec(code);

        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    @Override
    public User findOneByCodeAndRole(String code, UserRole role) {
        Specification<User> spec = new ByCodeSpec(code);

        if (role != null) {
            spec = spec.and(new ByRoleSpec(Collections.singleton(role)));
        }

        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }


    @Override
    @SuppressWarnings("null")
    public List<User> findAllByRole(EnumSet<UserRole> setRole, String fullname, Pageable pageable) {
        Specification<User> spec = new GetAllSpec();

        if (!CollectionUtils.isEmpty(setRole)) {
            spec = spec.and(new ByRoleSpec(setRole));
        }

        if (!StringUtils.isEmpty(fullname)) {
            spec = spec.and(new ByFullNameLikeSpec(fullname));
        }

        TypedQuery<User> query = getQuery(spec, Sort.unsorted());
        if (pageable != null) {
            query.setFirstResult((int) pageable.getOffset());
            query.setMaxResults(pageable.getPageSize());
        }
        return query.getResultList();
    }

    @Override
    public long count(EnumSet<UserRole> setRole, String fullname) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static class ByUsernameSpec implements Specification<User> {

        private final String username;

        public ByUsernameSpec(String username) {
            this.username = username;
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.equal(root.get("username"), username);
        }
    }

    public static class ByEmailSpec implements Specification<User> {

        private final String email;

        public ByEmailSpec(String email) {
            this.email = email;
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.equal(root.get("email"), email);
        }
    }

    public static class ByCodeSpec implements Specification<User> {

        private final String code;

        public ByCodeSpec(String code) {
            this.code = code;
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.equal(root.get("code"), code);
        }
    }

    public static class ByFullNameLikeSpec implements Specification<User> {

        @SuppressWarnings("FieldNameHidesFieldInSuperclass")
        private static final long serialVersionUID = 1L;

        private final String fullname;

        public ByFullNameLikeSpec(String fullname) {
            this.fullname = fullname;
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
            return criteriaBuilder.like(criteriaBuilder.lower(root.get("fullname")),
                    "%" + MyStringUtils.escapeWildcardsForSQL(fullname.toLowerCase()) + "%");
        }
    }

    public static class ByRoleSpec implements Specification<User> {

        @SuppressWarnings("FieldNameHidesFieldInSuperclass")
        private static final long serialVersionUID = 1L;

        private final Collection<UserRole> roles;

        public ByRoleSpec(Collection<UserRole> roles) {
            this.roles = roles;
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            if (CollectionUtils.isEmpty(roles)) {
                return null;
            }
            if (roles.size() > 1) {
                return root.get("userRole").in(roles);
            }
            return cb.equal(root.get("userRole"), roles.iterator().next());
        }
    }

    public static class GetAllSpec implements Specification<User> {

        public GetAllSpec() {
        }

        @Override
        public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            return cb.greaterThan(root.get("id"), 0);
        }
    }
}
