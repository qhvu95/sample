package com.vinatek.base.dto.base;

import com.vinatek.base.persistent.domain.base.PO;

public class DTO {

    private Long id;
    
    public DTO() {}

    public DTO(Long id) {
        super();

        this.id = id;
    }

    public DTO(PO po) {
        super();

        if (po.getId() != null) {
            this.id = po.getId();
        }
    }

    public Long getId() {
        return id;
    }

}
