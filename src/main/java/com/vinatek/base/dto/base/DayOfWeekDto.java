/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.dto.base;

import lombok.Data;

/**
 *
 * @author MyPC
 */
@Data
public class DayOfWeekDto {
    private Long id;
    private String name;
    
    public DayOfWeekDto(){
    
    }
    
    public DayOfWeekDto(Long id, String name){
        this.id = id;
        this.name = name;
    }
}
