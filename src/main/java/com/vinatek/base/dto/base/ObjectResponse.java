/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.dto.base;

import lombok.Data;

/**
 *
 * @author MyPC
 */
@Data
public class ObjectResponse {
    private int status;
    private String message;
    
    public ObjectResponse() {
    
    }
    
    public ObjectResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }
}
