package com.vinatek.base.dto.base;

import java.io.File;

public class FileDownloadDto {

    private final String fileName;
    private final File file;

    public FileDownloadDto(String fileName, File file) {
        super();
        this.fileName = fileName;
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public File getFile() {
        return file;
    }

}
