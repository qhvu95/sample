package com.vinatek.base.dto.user;

import com.vinatek.base.constant.UserRole;
import com.vinatek.base.dto.base.DTO;
import com.vinatek.base.persistent.domain.User;
import lombok.Data;

/**
 *
 * @author Khiem Ha
 */
@Data
public class UserDto extends DTO {

    private String username;
    private String name;
    private UserRole role;
    private int activeStatus;
    private Long createdBy;
    private Long createdAt;
    private Long updatedBy;
    private Long updatedAt;
    private String fcmToken;

    public UserDto() {
    }

    public UserDto(User domain) {
        super(domain.getId());
        this.username = domain.getUsername();
        this.name = domain.getName();
        this.role = domain.getUserRole();
        this.activeStatus = domain.getActiveStatus();
        this.createdBy = domain.getCreatedBy();
        this.createdAt = domain.getCreatedAt();
        this.updatedBy = domain.getUpdatedBy();
        this.updatedAt = domain.getUpdatedAt();
    }

}
