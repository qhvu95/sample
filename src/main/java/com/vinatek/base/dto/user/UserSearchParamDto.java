package com.vinatek.base.dto.user;

import com.vinatek.base.constant.UserRole;
import com.vinatek.base.dto.base.PageNumberRequestDto;
import java.util.EnumSet;
import lombok.Data;

/**
 *
 * @author Khiem Ha
 */
@Data
public class UserSearchParamDto extends PageNumberRequestDto {

    private EnumSet<UserRole> setRole;
    private String keyword;
}
