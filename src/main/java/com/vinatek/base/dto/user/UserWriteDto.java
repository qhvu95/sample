package com.vinatek.base.dto.user;

import com.vinatek.base.constant.ActiveStatus;
import com.vinatek.base.constant.UserRole;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Khiem Ha
 */
@Data
public class UserWriteDto {

    private Long id;
    private String username;
    private String name;
    private UserRole role;
    private ActiveStatus status;
    private String oldPassword;
    private String newPassword;
    private Long birthday;
    private String sex;
    private String address;
    private int state;
    private MultipartFile image;
    private String phone;
    private String fcmToken;
}
