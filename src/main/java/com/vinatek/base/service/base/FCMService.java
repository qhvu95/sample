/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.service.base;

import com.vinatek.base.dto.base.PnsRequest;
import org.springframework.stereotype.Service;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.vinatek.base.utils.DateTimeUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

/**
 *
 * @author MyPC
 */
@Service
public class FCMService extends A_Service {

    public void pushNotification(PnsRequest pnsRequest) {
        Message message = Message.builder()
                .setToken(pnsRequest.getFcmToken())
                .setNotification(new Notification(pnsRequest.getTitle(), pnsRequest.getBody()))
                .putData("content", pnsRequest.getTitle())
                .putData("body", pnsRequest.getBody())
                .build();
        String response = null;

        try {
            response = FirebaseMessaging.getInstance().send(message);
        } catch (FirebaseMessagingException ex) {
            response = ex.getMessage();
        }

        //return response;
    }

    public void sendMessage(String phone, String content) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://sms.donnhalaocai.com/tmsms.php");

        List<BasicNameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("sms_noidung", content));
        params.add(new BasicNameValuePair("sms_sdt", phone));
        httpPost.setEntity(new UrlEncodedFormEntity(params));

        client.execute(httpPost);
        client.close();
    }

//    public void sendNotifyHandle(String title, String content, int type, List<User> lstReceiver, Order order, OrderDetail orderDetail) throws IOException {
//
//        for (User user : lstReceiver) {
//            Notifications notification = new Notifications();
//            String message = "";
//            switch (type) {
//                case NotificationType.CREATED:
////                    String sms_content = "DT: " + order.getCustomer().getPhone() + ".\n"
////                            + "KH: " + order.getCustomer().getName() + ".\n"
////                            + "DC: " + order.getAddress().getAddress() + ".\n"
////                            + "Ngay dang ky: " + DateTimeUtils.formatDate(new Date(order.getCreatedAt())) + ".\n"
////                            + "Lich dat: " + getTime(order.getStartTime(), order.getEndTime())
////                            + " ngày " + DateTimeUtils.formatDate(new Date(order.getStartTime()));
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " vua duoc tao");
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " vừa được tạo!";
//                    break;
//                case NotificationType.ASIGN:
//                    List<Notifications> lstNotification = notificationRepository.search(order.getId(), null, NotificationType.CREATED, null, null);
//                    notificationRepository.deleteAll(lstNotification);
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " đã được xử lý!";
//                    sendMessage(user.getPhone(), getMessageContent(orderDetail));
//                    break;
//                case NotificationType.CUSTOMER_CANCEL:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " đã bị khách hàng hủy lịch!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " bi huy do khach hang huy lich.");
//                    break;
//                case NotificationType.CUSTOMER_CANCEL_ORDER_DETAIL:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " có 1 ca bị khách hàng hủy lịch!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " co 1 ca bi huy do khach hang huy lich.");
//                    break;
//                case NotificationType.OUT_OF_STAFF:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " đã bị hủy do hết nhân viên!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " bi huy do het nhan vien.");
//                    break;
//                case NotificationType.OUT_OF_STAFF_ORDER_DETAIL:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " có 1 ca bị hủy do hết nhân viên!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " co 1 ca bi huy do het nhan vien.");
//                    break;
//                case NotificationType.ASIGNED:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " vừa được giao cho bạn!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " vua duoc giao cho ban.");
//                    break;
//                case NotificationType.REMIND:
//                    message += "Bạn có lịch hẹn dọn nhà vào ngày mai.";
//                    sendMessage(user.getPhone(), "Ban co lich hen don nha vao luc " + getTime(orderDetail.getWorkTimeStart(), orderDetail.getWorkTimeEnd()) + " ngay mai.");
//                    break;
//                case NotificationType.STATISTICAL:
//                    message = content;
//                    sendMessage(user.getPhone(), content);
//                    break;
//                case NotificationType.CHANGE_PRICE:
//                    message += "Phiếu dịch vụ " + order.getOrderCode() + " vừa được cập nhật lại đơn giá!";
//                    sendMessage(user.getPhone(), "Phieu dich vu " + order.getOrderCode() + " vua duoc cap nhat lai don gia.");
//                    break;
//                case NotificationType.PAYMENT:
//                    message += "Bạn đã thanh toán số tiền " + content + " vào phiếu dịch vụ " + order.getOrderCode() + "!";
//                    sendMessage(user.getPhone(), "Ban da thanh toan so tien " + content + " vao phieu dich vu " + order.getOrderCode() + ".");
//                    break;
//
//            }
//            notification.setTitle(title);
//            notification.setMessage(message);
//            notification.setReceiverId(user.getId());
//            if (type != NotificationType.STATISTICAL) {
//                notification.setOrderId(order.getId());
//            }
//            notification.setCreatedAt(System.currentTimeMillis());
//            notification.setCreatedBy(null);
//            notification.setState(NotificationState.UNREAD);
//            notification.setType(type);
//            notificationRepository.save(notification);
//
//            if (!StringUtils.isEmpty(user.getFcmToken())) {
//                PnsRequest pnsRequest = new PnsRequest();
//                pnsRequest.setFcmToken(user.getFcmToken());
//                pnsRequest.setTitle(title);
//                pnsRequest.setBody(message);
//                pushNotification(pnsRequest);
//            }
//        }
//
//    }
//
//    public String getMessageContent(OrderDetail orderDetail) {
//        String message = "";
//        String lstStaffCode = "";
//        List<StaffWorkShift> lstStaffWorkShift = staffWorkShiftRepository.findAllByOrderDetailId(orderDetail.getId(), null);
//        for (StaffWorkShift staffWorkShift : lstStaffWorkShift) {
//            lstStaffCode += staffWorkShift.getStaff().getCode() + ",";
//        }
//        message += "TB: Dat lich thanh cong vao "
//                + getTime(orderDetail.getWorkTimeStart(), orderDetail.getWorkTimeEnd())
//                + " ngay " + DateTimeUtils.formatDate(new Date(orderDetail.getWorkTimeStart())) + ".\\n"
//                + "Ma nv: " + lstStaffCode + ".\\n"
//                + "Dich vu Don nha sach Lao cai - DT: 0981275018";
//
//        return message;
//    }

    public String getTime(Long fromTime, Long toTime) {
        Date fromDate = new Date(fromTime);
        Date toDate = new Date(toTime);
        return DateTimeUtils.formatTimeVn(fromDate) + "->" + DateTimeUtils.formatTimeVn(toDate);
    }
}
