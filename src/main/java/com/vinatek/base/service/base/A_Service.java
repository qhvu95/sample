package com.vinatek.base.service.base;

import com.vinatek.base.constant.UserRole;
// import com.vinatek.base.dto.base.ListDto;
import com.vinatek.base.dto.base.PageNumberRequestDto;
import com.vinatek.base.persistent.domain.User;
import com.vinatek.base.persistent.repository.UserRepository;
import com.vinatek.base.security.UserLogin;
import com.vinatek.base.utils.CodeUtils;
import com.vinatek.base.utils.DateTimeUtils;
import com.vinatek.base.utils.exception.BusinessAssert;
import com.vinatek.base.utils.exception.BusinessException;
import com.vinatek.base.utils.exception.BusinessExceptionCode;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Khiem Ha
 */
public class A_Service {

    @Autowired
    protected UserRepository userRepository;

    protected static final int PAGE_SIZE_DEFAULT = 20;
    protected static final BigDecimal DEFAULT_CHARGE_RATE = BigDecimal.valueOf(3);
    protected static final BigDecimal DEFAULT_RATE = BigDecimal.valueOf(3450);
    protected static final BigDecimal ONE_HUNDRED = BigDecimal.valueOf(100);
    protected static final BigDecimal ONE_HUNDRED_MILLION = BigDecimal.valueOf(100_000_000);
    protected static final BigDecimal FOUR_HUNDRED_MILLION = BigDecimal.valueOf(400_000_000);
    protected static final BigDecimal ONE_BILLION = BigDecimal.valueOf(1_000_000_000);
    protected static final BigDecimal TWO_BILLION = BigDecimal.valueOf(2_000_000_000);
    protected static final BigDecimal FIVE_BILLION = BigDecimal.valueOf(5_000_000_000L);
    protected static final BigDecimal TWENTY_BILLION = BigDecimal.valueOf(20_000_000_000L);

    protected Pageable getPageRequest(PageNumberRequestDto pageRequestDto) {
        int pageSize = pageRequestDto == null || pageRequestDto.getPageSize() == null ? PAGE_SIZE_DEFAULT : pageRequestDto.getPageSize();
        int pageNumber = pageRequestDto == null || pageRequestDto.getPageNumber() == null ? 0 : pageRequestDto.getPageNumber() - 1;
        return PageRequest.of(pageNumber, pageSize);
    }

    public String getUsernameLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    public UserLogin getUserLogin() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            User user = userRepository.findOneByUsername(username);
            if (user != null && user.getUserRole() != null) {
                return new UserLogin(user);
            }
        } catch (Exception ex) {

        }

        return null;
    }

    public User getUserLoginTypeUser() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            User user = userRepository.findOneByUsername(username);
            if (user != null && user.getUserRole() != null) {
                return user;
            }
        } catch (Exception ex) {

        }

        return null;
    }

    protected boolean isAdmin() {
        UserLogin userLogin = getUserLogin();
        return userLogin.isRole(UserRole.ROLE_ADMIN.name());
    }

//    protected boolean isCustomer() {
//        UserLogin userLogin = getUserLogin();
//        return userLogin.isRole(UserRole.ROLE_USER.name());
//    }
//
//    protected boolean isStaff() {
//        UserLogin userLogin = getUserLogin();
//        return userLogin.isRole(UserRole.ROLE_STAFF.name());
//    }
//
//    protected boolean isManagement() {
//        UserLogin userLogin = getUserLogin();
//        return userLogin.isRole(UserRole.ROLE_MANAGEMENT.name());
//    }

    protected String genUniqueCode(int lengt) {
        Boolean found = false;
        String result = CodeUtils.generateCodeOfGivenLength(lengt);
        while (!found) {
            User checkExist = userRepository.findOneByCode(result);
            if (checkExist != null) {
                result = CodeUtils.generateCodeOfGivenLength(lengt);
            } else {
                found = true;
            }
        }

        return result;
    }

    protected Date getMandatoryIsoDate(String isoDate) {
        BusinessAssert.notNull(isoDate);
        try {
            return DateTimeUtils.getDateFromIsoDate(isoDate);
        } catch (ParseException e) {
            throw new BusinessException(BusinessExceptionCode.INVALID_PARAM, "iso date wrong format");
        }
    }

    protected Date getMandatoryIsoTime(String isoTime) {
        BusinessAssert.notNull(isoTime);
        try {
            return DateTimeUtils.getDateFromIsoTime(isoTime);
        } catch (ParseException e) {
            throw new BusinessException(BusinessExceptionCode.INVALID_PARAM, "iso time wrong format");
        }
    }

    protected Date getDefaultTime(String defaultTime) {
        BusinessAssert.notNull(defaultTime);
        try {
            return DateTimeUtils.getDefaultTime(defaultTime);
        } catch (ParseException e) {
            throw new BusinessException(BusinessExceptionCode.INVALID_PARAM, "iso time wrong format");
        }
    }
    
    protected List<String> saveUploadedFiles(List<MultipartFile> files, String dir) throws IOException {
        List<String> lstFileImg = new ArrayList<>();
        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                continue; //next pls
            }
            lstFileImg.add(file.getOriginalFilename());
            byte[] bytes = file.getBytes();
            Path path = Paths.get(dir + file.getOriginalFilename());
            Files.write(path, bytes);
        }
        return lstFileImg;

    }

}
