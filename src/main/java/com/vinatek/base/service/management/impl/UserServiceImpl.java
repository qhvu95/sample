/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.service.management.impl;

import com.vinatek.base.constant.ActiveStatus;
import com.vinatek.base.constant.UserRole;
import com.vinatek.base.dto.base.ObjectResponse;
import com.vinatek.base.dto.user.UserDto;
import com.vinatek.base.dto.user.UserWriteDto;
import com.vinatek.base.persistent.domain.User;
import com.vinatek.base.security.UserLogin;
import com.vinatek.base.service.base.A_Service;
import com.vinatek.base.service.management.UserService;
import com.vinatek.base.utils.CodeUtils;
import com.vinatek.base.utils.exception.BusinessAssert;
import com.vinatek.base.utils.exception.BusinessExceptionCode;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

/**
 *
 * @author Khiem Ha
 */
@Service
@Transactional
public class UserServiceImpl extends A_Service implements UserService {

    private static final String DEFAULT_PASSWORD = "123456";

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Override
    public UserLogin getCurrentUser() {
        UserLogin userLogin = getUserLogin();
        BusinessAssert.notNull(userLogin);

        return userLogin;
    }

    @Override
    public UserDto registerCustomer(UserWriteDto request) {
        BusinessAssert.notNull(request);
        BusinessAssert.notNull(request.getUsername());

        User checkExistUsername = userRepository.findOneByUsername(request.getUsername());
        BusinessAssert.isTrue(checkExistUsername == null, BusinessExceptionCode.USERNAME_USED, "phone number existed");

        User newUser = new User();
        newUser.setUsername(request.getUsername());
        String encryptedPassword = passwordEncoder.encode(DEFAULT_PASSWORD);
        newUser.setPassword(encryptedPassword);
        newUser.setName(request.getName());
        newUser.setUserRole(UserRole.ROLE_ADMIN);
        newUser.setActiveStatus(ActiveStatus.ACTIVE);
        newUser.setCreatedAt(System.currentTimeMillis());
        newUser = userRepository.save(newUser);
        
        return new UserDto(newUser);
    }

    @Override
    public UserDto create(UserWriteDto request) {
        BusinessAssert.notNull(request);
        BusinessAssert.notNull(request.getUsername());
        BusinessAssert.notNull(request.getName());
        BusinessAssert.notNull(request.getRole());
        UserLogin userLogin = getUserLogin();
        BusinessAssert.notNull(request.getNewPassword());
        BusinessAssert.isTrue(isAdmin(), BusinessExceptionCode.PERMISSION_DENIED, "permission denied");
        User checkExistUsername = userRepository.findOneByUsername(request.getUsername());
        BusinessAssert.isTrue(checkExistUsername == null, BusinessExceptionCode.USERNAME_USED, "username existed");

        User newUser = new User();
        newUser.setUsername(request.getUsername());
        String encryptedPassword = passwordEncoder.encode(request.getNewPassword());
        newUser.setPassword(encryptedPassword);
        newUser.setName(request.getName());
        newUser.setUserRole(request.getRole());
        newUser.setActiveStatus(ActiveStatus.ACTIVE);
        newUser.setCreatedAt(System.currentTimeMillis());
        newUser.setCreatedBy(userLogin.getId());
        newUser = userRepository.save(newUser);
        
        return new UserDto(newUser);
    }

    @Override
    public UserDto updateUser(UserWriteDto request) {
        BusinessAssert.notNull(request);
        BusinessAssert.notNull(request.getId());

        UserLogin userLogin = getUserLogin();
        BusinessAssert.isTrue(isAdmin(), BusinessExceptionCode.PERMISSION_DENIED, "permission denied");
        Optional<User> optionalUser = userRepository.findById(request.getId());
        BusinessAssert.isTrue(optionalUser.isPresent());
        User user = optionalUser.get();
        if (request.getRole() != null) {
            user.setUserRole(request.getRole());
        }
        if (request.getStatus() != null) {
            user.setActiveStatus(ActiveStatus.ACTIVE);
        }

        user.setUpdatedBy(userLogin.getId());

        user = userRepository.save(user);
        return new UserDto(user);
    }

    @Override
    public UserDto updateFcmToken(UserWriteDto request) {
        BusinessAssert.notNull(request);

        UserLogin userLogin = getUserLogin();
        User user = userRepository.findOneByUsername(userLogin.getUsername());
        BusinessAssert.notNull(user);

        if (!StringUtils.isEmpty(request.getFcmToken())) {
            user.setFcmToken(request.getFcmToken());
        }

        user = userRepository.save(user);
        return new UserDto(user);
    }

    @Override
    public UserDto getById(Long id) {
        BusinessAssert.notNull(id);
        UserLogin userLogin = getUserLogin();
        // BusinessAssert.isTrue(!isCustomer(), BusinessExceptionCode.PERMISSION_DENIED, "permission denied");

        Optional<User> optionalUser = userRepository.findById(id);
        BusinessAssert.isTrue(optionalUser.isPresent(), BusinessExceptionCode.USERNAME_NOT_FOUND, "user not found");
        User user = optionalUser.get();

        return new UserDto(user);
    }

    @Override
    public void delete(Long id) {
        BusinessAssert.isTrue(isAdmin(), BusinessExceptionCode.PERMISSION_DENIED, "permission denied");
        BusinessAssert.notNull(id);
        userRepository.deleteById(id);
    }
    

    @Override
    public ObjectResponse changePassword(UserWriteDto request) {
        BusinessAssert.notNull(request);
        BusinessAssert.notNull(request.getId());
        BusinessAssert.notNull(request.getOldPassword());
        BusinessAssert.notNull(request.getNewPassword());
        Optional<User> optionalUser = userRepository.findById(request.getId());
        BusinessAssert.isTrue(optionalUser.isPresent(), BusinessExceptionCode.USERNAME_NOT_FOUND, "user not found");
        User user = optionalUser.get();
        String currentPass = user.getPassword();
        BusinessAssert.isTrue(passwordEncoder.matches(request.getOldPassword(), currentPass),
                BusinessExceptionCode.INVALID_OLD_PASSWORD, "wrong oldPass");
        String encryptedPassword = passwordEncoder.encode(request.getNewPassword());
        user.setPassword(encryptedPassword);
        user.setUpdatedBy(getUserLogin().getId());
        
        userRepository.save(user);
        return new ObjectResponse(200, "SUCCESS");
    }

    String genCode(UserRole role) {
        String value = "";
        switch(role) {
            case ROLE_ADMIN: {
                String code = CodeUtils.generateCodeOfGivenLength(6);
                value = "ADMIN" + "_" + code;
                break;
            }
        }
        return value;
    }
    
    @Override
    public ObjectResponse resetPassword(Long id) {
        BusinessAssert.notNull(id);
        Optional<User> optionalUser = userRepository.findById(id);
        BusinessAssert.isTrue(optionalUser.isPresent(), BusinessExceptionCode.USERNAME_NOT_FOUND, "user not found");
        User user = optionalUser.get();
        String encryptedPassword = passwordEncoder.encode(DEFAULT_PASSWORD);
        user.setPassword(encryptedPassword);
        user.setUpdatedBy(getUserLogin().getId());
        
        userRepository.save(user);
        return new ObjectResponse(200, "SUCCESS");
    }
}
