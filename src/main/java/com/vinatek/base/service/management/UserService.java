/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vinatek.base.service.management;

import com.vinatek.base.dto.base.ObjectResponse;
import com.vinatek.base.dto.user.UserDto;
import com.vinatek.base.dto.user.UserWriteDto;
import com.vinatek.base.security.UserLogin;

/**
 *
 * @author Khiem Ha
 */
public interface UserService {

    UserLogin getCurrentUser();
    
    UserDto registerCustomer(UserWriteDto request);
    
    UserDto create(UserWriteDto request);
    
    UserDto updateUser(UserWriteDto request);
    
    UserDto getById(Long id);
    
    void delete(Long id);
    
    ObjectResponse changePassword(UserWriteDto request);
    
    UserDto updateFcmToken(UserWriteDto request);
    
    ObjectResponse resetPassword(Long id);
}
